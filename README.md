Kakoune-GTK
===========

A proof-of-concept for a Kakoune user-interface based on GTK.

Requirements
------------

  - Python 3.5+
  - Python GObject Introspection libraries (in Debian, `python3-gi`)
  - GObject Introspection libraries for GLib, Gio, Gdk, Pango, PangoCairo and
    Gtk (in Debian, packages named like `gir1.2-*`)
  - Python bindings for Cairo (in Debian, `python3-cairo`)
  - Kakoune installed on `$PATH`

Features
--------

  - Choose a font by setting the `gtk_font` key in the `ui_options` option to
    the font name, like this:

        set -add global ui_options "gtk_font=DejaVu Sans Mono 9"

    If the font isn't monospaced, we'll do our best to make it look nice anyway,
    but it'll never be quite as nice as natural proportionally-spaced output.
  - Renders Kakoune's output with the Pango library, so it supports all the
    fancy ligatures from fonts like Hasklig, Monoid, and Iosevka
  - Supports more modifiers than terminals can support. For example, `<c-tab>`
    is now mappable
  - When the mode-line is too wide for the screen, or when the prompt is
    visible, the overlap is shown by a smooth fadeout rather than a simple
    ellipsis
  - Supports scrolling the completion menu with the scroll-wheel, and selecting
    items with the mouse,

Broken things
-------------

  - No support for inline, menu or modal info panels.
  - No control-click to add a cursor.
  - Prompt info panels truncate the title to fit the content, instead of
    expanding the panel to fit both.
  - No support for the "blink" and "reverse" text attributes.
  - Assumes each Unicode character occupies 1 character cell, which is Blatantly
    Wrong
  - Named colours use a hard-coded palette (currently the Tango terminal
    palette)
  - Mappings like `<c-s-a>` should be possible, but aren't because our keyboard
    handling is still quite hacky
  - No clipboard integration, or X11 selection integration
  - No support for `:new`, how would that even work?
  - Ideally, should load a `gkakrc` at startup, by analogy to `.gvimrc`.
  - Open files by drag-and-drop.
  - There should be a toolbar with buttons like Open, Save, Copy, Paste,
    New Window
  - A tab-strip with open files?
  - Build on GtkApplication so files opened from the file-manager (etc.)
    automatically open in a new window attached to the same session.
  - The completion menu can get into a state where hitting the key to cycle
    through the options winds up automatically selecting the first item.

Installation
------------

  - Copy `gkak.py` to somewhere on `$PATH`
  - Run it!

Usage
-----

`gkak` passes all of its command-line arguments through to Kakoune, so you can
run it like you normally would run Kakoune.

